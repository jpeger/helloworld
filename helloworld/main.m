//
//  main.m
//  helloworld
//
//  Created by Stephen Bromley on 1/12/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
