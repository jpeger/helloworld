//
//  AppDelegate.h
//  helloworld
//
//  Created by Stephen Bromley on 1/12/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

