//
//  ViewController.m
//  helloworld
//
//  Created by Stephen Bromley on 1/12/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView* sv = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    sv.backgroundColor = [UIColor redColor];
    [sv setContentSize:CGSizeMake(self.view.frame.size.width * 9,self.view.frame.size.height)];
    
    [self.view addSubview:sv];
    
    for (int i = 0; i<9; i++) {
        UIView* view = [[UIView alloc]initWithFrame:CGRectMake(i * self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
        UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(50, 50, 100, 100)];
        
        switch (i) {
            case 0:
                view.backgroundColor = [UIColor blueColor];
                [lbl setText:@"Blue"];
                [view addSubview:lbl];
                break;
            case 1:
                view.backgroundColor = [UIColor greenColor];
                [lbl setText:@"Green"];
                [view addSubview:lbl];
                break;
            case 2:
                view.backgroundColor = [UIColor redColor];
                [lbl setText:@"Red"];
                [view addSubview:lbl];
                break;
            case 3:
                view.backgroundColor = [UIColor yellowColor];
                [lbl setText:@"Yellow"];
                [view addSubview:lbl];
                break;
            case 4:
                view.backgroundColor = [UIColor grayColor];
                [lbl setText:@"Gray"];
                [view addSubview:lbl];
                break;
            case 5:
                view.backgroundColor = [UIColor brownColor];
                [lbl setText:@"Brown"];
                [view addSubview:lbl];
                break;
            case 6:
                view.backgroundColor = [UIColor purpleColor];
                [lbl setText:@"Purple"];
                [view addSubview:lbl];
                break;
            case 7:
                view.backgroundColor = [UIColor orangeColor];
                [lbl setText:@"Orange"];
                [view addSubview:lbl];
                break;
            case 8:
                view.backgroundColor = [UIColor cyanColor];
                [lbl setText:@"Cyan"];
                [view addSubview:lbl];
                break;
            default:
                break;
        }
        [sv setPagingEnabled:YES];
        [sv addSubview:view];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
